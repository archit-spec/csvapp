import sys
import pandas as pd
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QVBoxLayout, QWidget, QFileDialog, QTableWidget, QTableWidgetItem, QLabel

class CSVSortApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("CSV Sorting App")
        self.setGeometry(100, 100, 800, 600)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.label = QLabel("No files loaded")
        self.layout.addWidget(self.label)

        self.table = QTableWidget()
        self.layout.addWidget(self.table)

        self.load_button = QPushButton("Load CSVs")
        self.load_button.clicked.connect(self.load_csvs)
        self.layout.addWidget(self.load_button)

        self.sort_price_button = QPushButton("Sort by Price")
        self.sort_price_button.clicked.connect(self.sort_by_price)
        self.layout.addWidget(self.sort_price_button)

        self.sort_brand_button = QPushButton("Sort by Brand")
        self.sort_brand_button.clicked.connect(self.sort_by_brand)
        self.layout.addWidget(self.sort_brand_button)

        self.sort_type_button = QPushButton("Sort by Product Type")
        self.sort_type_button.clicked.connect(self.sort_by_type)
        self.layout.addWidget(self.sort_type_button)

        self.filter_lowest_button = QPushButton("Filter Lowest Prices")
        self.filter_lowest_button.clicked.connect(self.filter_lowest_prices)
        self.layout.addWidget(self.filter_lowest_button)

        self.df = None

    def load_csvs(self):
        file_paths, _ = QFileDialog.getOpenFileNames(self, "Open CSVs", "", "CSV Files (*.csv);;All Files (*)")
        if file_paths:
            dfs = [pd.read_csv(file_path) for file_path in file_paths]
            self.df = pd.concat(dfs, ignore_index=True)
            self.label.setText(f"Loaded {len(file_paths)} files")
            self.display_data(self.df)

    def display_data(self, df):
        self.table.setRowCount(df.shape[0])
        self.table.setColumnCount(df.shape[1])
        self.table.setHorizontalHeaderLabels(df.columns)

        for row in range(df.shape[0]):
            for col in range(df.shape[1]):
                item = QTableWidgetItem(str(df.iat[row, col]))
                self.table.setItem(row, col, item)

    def sort_by_price(self):
        if self.df is not None and 'price' in self.df.columns:
            sorted_df = self.df.sort_values(by='price')
            self.display_data(sorted_df)
        else:
            self.label.setText("Column 'price' does not exist in the DataFrame.")

    def sort_by_brand(self):
        if self.df is not None and 'brand' in self.df.columns:
            sorted_df = self.df.sort_values(by='brand')
            self.display_data(sorted_df)
        else:
            self.label.setText("Column 'brand' does not exist in the DataFrame.")

    def sort_by_type(self):
        if self.df is not None and 'product_type' in self.df.columns:
            sorted_df = self.df.sort_values(by='product_type')
            self.display_data(sorted_df)
        else:
            self.label.setText("Column 'product_type' does not exist in the DataFrame.")

    def filter_lowest_prices(self):
        if self.df is not None and 'price' in self.df.columns:
            lowest_prices_df = self.df.sort_values(by='price').drop_duplicates(subset=['brand', 'product_type'])
            self.display_data(lowest_prices_df)
        else:
            self.label.setText("Column 'price' does not exist in the DataFrame.")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = CSVSortApp()
    window.show()
    sys.exit(app.exec_())
